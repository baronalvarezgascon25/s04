/*Demo*/
document.getElementById("btn-1").addEventListener('click', () => {
	alert("Add More!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
	paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "purple";
	paragraph2.style.fontSize = "50px";
});

/*
	Javascript
		- is a scripting language the enables you to make interactive web pages		
*/
console.log("Hello, my name is Baron.");
/*
	Variables
	In JavaScript, variables are containers of data.
*/
let num = 10;

console.log(6);
console.log(num);

let name = "Jin";
console.log("V");
console.log(name);
/*
	Creating Variables
	To create a variable, there are two steps to be done:
		- Declaration 
		- Initiliazation
*/
let myVariable;
console.log(myVariable);
myVariable = "New initialized value";
console.log(myVariable);

let myVariable3 = "Another sample";
console.log(myVariable3);
myVariable3 = "A renewed sample";
console.log(myVariable3);

const pi = 3.1416;
console.log(pi);
/*
	const variables are variables with constant data.
*/
const mvp = "Michael Jordan";
console.log(mvp);

let num_sum = 5000;
let numSum = 6000;

console.log(num_sum);
console.log(numSum);
// Declaring multiple variables
let brand = "Toyota", model = "Vios", type = "Sedan";
console.log(brand);
console.log(model);
console.log(type);
// console logging multiple variables: use commas to separate each variable
console.log(brand, model, type);

/*
	Data Types
	literals
		string literals = '',"",``
		object literals = {}
		array literals = []
*/
let country = "Philippines";
let province = 'Metro Manila';

console.log(country);
console.log(province);

/*
	Mini Activity
*/
let firstName = "Baron";
let lastName = "Gascon";
console.log(firstName, lastName);

/*
	Concatenation
*/
let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = "De La Salle";
let word6 = "a";
let word7 = "Dasmarinas";
let space = " ";
/*
	Mini-Activity
*/
let sentence;
sentence=firstName+space+lastName+space+word1+space+word6+space+
word2+space+word3+space+word5+space+word4+space+word7;
console.log(sentence);
// Template Literals
sentence = `${firstName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word4} ${word7}`;
console.log(sentence);

let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
console.log(numString1+numString2);

console.log(num1+num2);

let num3 = 5.5;
let num4 = .5;
console.log(num1+num3);
console.log(num3+num4);

// Forced coercion
console.log(numString1 + num1);
console.log(num3 + numString2);

//parseInt()
console.log(parseInt(numString1) + num1);